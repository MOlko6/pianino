// Aleksandra, Matylda
package pl.edu.pw.fizyka.pojava.NeumannOlko;

import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
//import javafx.scene.media.Media;
//import javafx.scene.media.MediaPlayer;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import pl.edu.pw.fizyka.pojava.NeumannOlko.Klawisz.TypKlawisza;

public class OknoGraj extends JFrame {

	public OknoGraj() throws HeadlessException {
		this.setSize(640, 480);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		
		JMenu menu = new JMenu("Menu");
		
		JMenuItem autor = new JMenuItem("Autorzy");
		autor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane okienkoDialogowe = new JOptionPane();
				okienkoDialogowe.showMessageDialog(OknoGraj.this, "Aleksandra Neumann, Matylda Olko", "Autorzy", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		menu.add(autor);
		menuBar.add(menu);
		this.setJMenuBar(menuBar);
	}
}
