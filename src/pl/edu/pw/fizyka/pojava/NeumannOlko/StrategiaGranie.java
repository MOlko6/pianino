// Aleksandra, Matylda
package pl.edu.pw.fizyka.pojava.NeumannOlko;

import java.awt.Color;
import java.awt.event.MouseEvent;

public class StrategiaGranie extends StrategiaGry {
	public void mousePressed(MouseEvent e) {
    	for (int i = 0; i < piano.keyboard.length; i++) {
    		if (piano.keyboard[i].contains(e.getPoint())) {
    			piano.playSound(piano.soundsDir + "/" + piano.sounds[i], volume);
    			piano.keyboard[i].setKolorKlawisza(Color.green);
    			piano.repaint();
    		}
    	}  	
    }
    
    public void mouseReleased(MouseEvent e) {
    	for (int i = 0; i < piano.keyboard.length; i++) {
    		piano.keyboard[i].setKolorKlawisza(piano.keyboard[i].getDomyslnyKolor());
    	}
    	piano.repaint();
    }
}
