// Matylda
package pl.edu.pw.fizyka.pojava.NeumannOlko;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.swing.JPanel;

import pl.edu.pw.fizyka.pojava.NeumannOlko.Klawisz.TypKlawisza;

public class Klawiatura extends JPanel {
	TypKlawisza[] pattern = {TypKlawisza.CL, TypKlawisza.BO, TypKlawisza.CO, TypKlawisza.BL,TypKlawisza.BP, TypKlawisza.CO, TypKlawisza.BO, 
			TypKlawisza.CO, TypKlawisza.BL, TypKlawisza.BP, TypKlawisza.CO, TypKlawisza.BO, 
			TypKlawisza.CO, TypKlawisza.BO, TypKlawisza.CO, TypKlawisza.BL, TypKlawisza.BP, TypKlawisza.CO, TypKlawisza.BO, 
			TypKlawisza.CO, TypKlawisza.BL, TypKlawisza.BP, TypKlawisza.CO, TypKlawisza.BO, TypKlawisza.CP};
	
	String soundsDir = "dzwieki";
	String[] sounds = {"gis.wav", "am.wav", "aism.wav", "h.wav", "c1.wav", "cis1.wav", "d1.wav", "dis1.wav", "e1.wav", "f1.wav", "fis1.wav", "g1.wav", "gis1.wav", "a1.wav", "ais1.wav", "h1.wav", "c2.wav",
			"cis2.wav", "d2.wav", "dis2.wav", "e2.wav", "f2.wav", "fis2.wav", "g2.wav", "gis2.wav"};
	String[] names = {"G#", "A", "A#", "H", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "H", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"};
	int iloscBialychKlawiszy;
	Klawisz[] keyboard;
	
	boolean renderNames = false;

	public void setRenderNames(boolean renderNames) {
		this.renderNames = renderNames;
	}

	public Klawiatura(StrategiaGry strategia) throws HeadlessException {
		iloscBialychKlawiszy = 0;
		for (int i = 0; i < pattern.length; i++) {
			if ((pattern[i] == Klawisz.TypKlawisza.BL) || (pattern[i] == Klawisz.TypKlawisza.BP) || (pattern[i] == Klawisz.TypKlawisza.BO)) iloscBialychKlawiszy++;
		}
		
		// Inicjalizacja klawiatury - tablicy klawiszy
		keyboard = new Klawisz[pattern.length];
		for (int i = 0; i < pattern.length; i++) {
			keyboard[i] = new Klawisz(pattern[i]);
		}
		
		// Reakcja odpowiedniego klawisza na klikni�cie myszy
		strategia.setPiano(this); // przekazuj� t� klawiatur� do strategii, �eby mie� dost�p do listy dzwi�k�w, odtwarzania i malowania klawiatury
		addMouseListener(strategia);
	}
	
	// Rysowanie klawiatury
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int szerokoscKlawisza = getSize().width / iloscBialychKlawiszy;
		int wysokoscKlawisza = getSize().height;
		int punktZaczepieniaX = 0;
		int punktZaczepieniaY = 0;
		
		for (int i = 0; i < pattern.length; i++) {
			keyboard[i].paint(g, punktZaczepieniaX, punktZaczepieniaY, wysokoscKlawisza, szerokoscKlawisza, renderNames ? names[i] : null);
			if ((pattern[i] == Klawisz.TypKlawisza.BL) || (pattern[i] == Klawisz.TypKlawisza.BP) || (pattern[i] == Klawisz.TypKlawisza.BO)) punktZaczepieniaX += szerokoscKlawisza;
		}
	}
	
	// Metoda odtwarzaj�ca dzwi�k
	public void playSound(String fileName, int volumeChange) {
		try {
		    File yourFile = new File(fileName);
		    AudioInputStream stream;
		    AudioFormat format;
		    DataLine.Info info;
		    Clip clip;
	        
		    stream = AudioSystem.getAudioInputStream(yourFile);
		    format = stream.getFormat();
		    info = new DataLine.Info(Clip.class, format);
		    clip = (Clip) AudioSystem.getLine(info);
		    clip.open(stream);
		    
		    // Ustawienia g�o�no�ci
		    FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
		    float minimumVolume = (float) Math.max(gainControl.getMinimum(), -32.0);
		    float maximumVolume = (float) Math.min(gainControl.getMaximum(), 12.0);
		    float volume = (((float)volumeChange) / 100) * (maximumVolume - minimumVolume) + minimumVolume;
		    gainControl.setValue(volume);
		    
		    clip.start();
		}
		catch (Exception e) {
		    System.out.println(e);
		}
	}
}
