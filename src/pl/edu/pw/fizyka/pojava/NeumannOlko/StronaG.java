// Aleksandra, Matylda
package pl.edu.pw.fizyka.pojava.NeumannOlko;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GraphicsConfiguration;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import pl.edu.pw.fizyka.pojava.NeumannOlko.Klawisz.TypKlawisza;

public class StronaG extends JFrame {
		
	private JPanel p1, p2 /*, p3*/;
	private JButton play, study /*, pl, eng*/;
	
	public StronaG() throws HeadlessException {
		super();
		setSize(640, 480);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		
		JMenuBar menuBar = new JMenuBar();
		
		JMenu menu = new JMenu("Menu");
		
		JMenuItem autor = new JMenuItem("Autorzy");
		autor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane okienkoDialogowe = new JOptionPane();
				okienkoDialogowe.showMessageDialog(StronaG.this, "Aleksandra Neumann, Matylda Olko", "Autorzy", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		menu.add(autor);
		menuBar.add(menu);
		this.setJMenuBar(menuBar);
		
		p1 = new JPanel();
		p2 = new JPanel();
//		p3 = new JPanel();
		
	   add(BorderLayout.NORTH, p1);
	   add(BorderLayout.CENTER, p2);
//	   add(BorderLayout.SOUTH, p3);
	   
	   p1.setLayout(new FlowLayout());
	   p1.add(new JLabel("Wybierz opcje"));
	   p1.setBackground(Color.white);
	   
	   play = new JButton("GRAJ");
	   play.setPreferredSize(new Dimension(180, 120));
	   play.setIcon(new ImageIcon("obrazki/buttong.jpg"));
	   play.setHorizontalTextPosition(JLabel.CENTER);
	   play.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
					OknoGraj okno = new OknoGraj();
					StrategiaGranie strategia = new StrategiaGranie();
					Klawiatura klawiatura = new Klawiatura(strategia);
					Graj graj = new Graj(strategia);
					
					okno.setLayout(new GridLayout(2, 1));
					okno.add(graj);
					okno.add(klawiatura);
					
					okno.setVisible(true);
					dispose();
			}
		});
	   
	   study = new JButton("UCZ SIE");
	   study.setPreferredSize(new Dimension(180, 120));
	   study.setIcon(new ImageIcon("obrazki/buttonu.jpg"));
	   study.setHorizontalTextPosition(JLabel.CENTER);
	   study.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				OknoUcz okno = new OknoUcz();
				StrategiaNauka strategia = new StrategiaNauka();
				Klawiatura klawiatura = new Klawiatura(strategia);
				Ucz ucz = new Ucz(strategia); // przekazuj� strategi�, �eby mo�na by�o ustawi� melodi�
				
				okno.setLayout(new GridLayout(2, 1));
				okno.add(ucz);
				okno.add(klawiatura);
				
				okno.setVisible(true);
				dispose();
			}
		});
	   
	   p2.add(play);
	   p2.add(study);
	   p2.setBackground(Color.white);
	   JLabel background = new JLabel();
	   background.setIcon(new ImageIcon("obrazki/music.jpg"));
	   p2.add(background);
	   
//	   p3.setLayout((new GridLayout(1,2)));
//	   pl = new JButton("PL");
//	   eng = new JButton("ENG");
//	   p3.add(pl);
//	   p3.add(eng);
	}

	public StronaG(GraphicsConfiguration gc) {
		super(gc);
	}

	public StronaG(String title) throws HeadlessException {
		super(title);
	}

	public StronaG(String title, GraphicsConfiguration gc) {
		super(title, gc);
	}

	public static void main(String[] args) {
		JFrame f = new StronaG();
		f.setVisible(true);
	}

}


