// Aleksandra, Matylda
package pl.edu.pw.fizyka.pojava.NeumannOlko;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.Polygon;

public class Klawisz {
	public enum TypKlawisza {
		BL, // bia�y z wyci�ciem z lewej strony
		BP, // bia�y z wyci�ciem z prawej strony
		BO, // bia�y z wyci�ciem z obu stron
		CL, // czarny po lewej stronie bia�ego
		CP, // czarny po prawej stronie bia�ego
		CO // czarny na ��czeniu dw�ch klawiszy
	}
	
	final TypKlawisza typ;
	final Color domyslnyKolor;
	Color kolorKlawisza;
	Polygon key;
	
	public Color getDomyslnyKolor() {
		return domyslnyKolor;
	}
	
	public Color getKolorKlawisza() {
		return kolorKlawisza;
	}

	public void setKolorKlawisza(Color kolorKlawisza) {
		this.kolorKlawisza = kolorKlawisza;
	}

	// Konstruktor
	public Klawisz(TypKlawisza t) throws HeadlessException {
		typ = t;
		if ((typ == TypKlawisza.BL) || (typ == TypKlawisza.BP) || (typ == TypKlawisza.BO)) domyslnyKolor = Color.white;
		else domyslnyKolor = Color.black;
		kolorKlawisza = domyslnyKolor;
	}
	
	// Czy dany punkt nale�y do klawisza (wykorzystywane przy klikaniu mysz� w klasie "Klawiatura")
	public boolean contains(Point point) {
		return key.contains(point);
	}
	
	// Metoda rysuj�ca klawisz
	public void paint(Graphics g, int punktOdniesieniaX, int punktOdniesieniaY, int wysokosc, int szerokosc, String name) {
		
		int liczbaWierzcholkow = 0;
		int wspolrzedneX[] = null;
		int wspolrzedneY[] = null;
		int nameX = 0;
		int nameY = 0;
		int stringWidth = 0;
		if (name != null) {
			stringWidth = g.getFontMetrics().stringWidth(name);
		}
		
		// Odpowiednie kszta�ty kolejnych klawiszy
		if (typ == TypKlawisza.BL) {
			liczbaWierzcholkow = 6;
			wspolrzedneX = new int[liczbaWierzcholkow];
			wspolrzedneY = new int[liczbaWierzcholkow];
			
			wspolrzedneX[0] = szerokosc / 3;
			wspolrzedneX[1] = szerokosc;
			wspolrzedneX[2] = szerokosc;
			wspolrzedneX[3] = 0;
			wspolrzedneX[4] = 0;
			wspolrzedneX[5] = szerokosc / 3;
			
			wspolrzedneY[0] = 0;
			wspolrzedneY[1] = 0;
			wspolrzedneY[2] = wysokosc;
			wspolrzedneY[3] = wysokosc;
			wspolrzedneY[4] = wysokosc / 2;
			wspolrzedneY[5] = wysokosc / 2;
			
			nameX = - stringWidth / 2 + szerokosc / 2;
			nameY = 4 * wysokosc / 5;
		}
		
		if (typ == TypKlawisza.BP) {
			liczbaWierzcholkow = 6;
			wspolrzedneX = new int[liczbaWierzcholkow];
			wspolrzedneY = new int[liczbaWierzcholkow];
			
			wspolrzedneX[0] = 0;
			wspolrzedneX[1] = 2 * szerokosc / 3;
			wspolrzedneX[2] = 2 * szerokosc / 3;
			wspolrzedneX[3] = szerokosc;
			wspolrzedneX[4] = szerokosc;
			wspolrzedneX[5] = 0;
			
			wspolrzedneY[0] = 0;
			wspolrzedneY[1] = 0;
			wspolrzedneY[2] = wysokosc / 2;
			wspolrzedneY[3] = wysokosc / 2;
			wspolrzedneY[4] = wysokosc;
			wspolrzedneY[5] = wysokosc;
			
			nameX = - stringWidth / 2 + szerokosc / 2;
			nameY = 4 * wysokosc / 5;
		}
		
		if (typ == TypKlawisza.BO) {
			liczbaWierzcholkow = 8;
			wspolrzedneX = new int[liczbaWierzcholkow];
			wspolrzedneY = new int[liczbaWierzcholkow];
			
			wspolrzedneX[0] = szerokosc / 3;
			wspolrzedneX[1] = 2* szerokosc / 3;
			wspolrzedneX[2] = 2* szerokosc / 3;
			wspolrzedneX[3] = szerokosc;
			wspolrzedneX[4] = szerokosc;
			wspolrzedneX[5] = 0;
			wspolrzedneX[6] = 0;
			wspolrzedneX[7] = szerokosc / 3;
			
			wspolrzedneY[0] = 0;
			wspolrzedneY[1] = 0;
			wspolrzedneY[2] = wysokosc / 2;
			wspolrzedneY[3] = wysokosc / 2;
			wspolrzedneY[4] = wysokosc;
			wspolrzedneY[5] = wysokosc;
			wspolrzedneY[6] = wysokosc / 2;
			wspolrzedneY[7] = wysokosc /2;
			
			nameX = - stringWidth / 2 + szerokosc / 2;
			nameY = 4 * wysokosc / 5;

		}
		
		if (typ == TypKlawisza.CL) {
			liczbaWierzcholkow = 4;
			wspolrzedneX = new int[liczbaWierzcholkow];
			wspolrzedneY = new int[liczbaWierzcholkow];
			
			wspolrzedneX[0] = 0;
			wspolrzedneX[1] = szerokosc / 3;
			wspolrzedneX[2] = szerokosc / 3;
			wspolrzedneX[3] = 0;
			
			wspolrzedneY[0] = 0;
			wspolrzedneY[1] = 0;
			wspolrzedneY[2] = wysokosc / 2;
			wspolrzedneY[3] = wysokosc / 2;
			
			nameX = - stringWidth / 2 + szerokosc / 6;
			nameY =  wysokosc * 3 / 10;
		}
		
		if (typ == TypKlawisza.CP) {
			liczbaWierzcholkow = 4;
			wspolrzedneX = new int[liczbaWierzcholkow];
			wspolrzedneY = new int[liczbaWierzcholkow];
			
			wspolrzedneX[0] = -szerokosc / 3;
			wspolrzedneX[1] = 0;
			wspolrzedneX[2] = 0;
			wspolrzedneX[3] = -szerokosc / 3;
	
			wspolrzedneY[0] = 0;
			wspolrzedneY[1] = 0;
			wspolrzedneY[2] = wysokosc / 2;
			wspolrzedneY[3] = wysokosc / 2;
			
			nameX = - stringWidth / 2 - szerokosc / 6;
			nameY =  wysokosc * 3 / 10;
		}
		
		if (typ == TypKlawisza.CO) {
			liczbaWierzcholkow = 4;
			wspolrzedneX = new int[liczbaWierzcholkow];
			wspolrzedneY = new int[liczbaWierzcholkow];
			
			wspolrzedneX[0] = -szerokosc / 3;
			wspolrzedneX[1] = szerokosc / 3;
			wspolrzedneX[2] = szerokosc / 3;
			wspolrzedneX[3] = -szerokosc /3;
			
			wspolrzedneY[0] = 0;
			wspolrzedneY[1] = 0;
			wspolrzedneY[2] = wysokosc / 2;
			wspolrzedneY[3] = wysokosc / 2;
			
			nameX = - stringWidth / 2;
			nameY =  wysokosc * 3 / 10;
		}
		
		for (int i = 0; i < liczbaWierzcholkow; i++) {
			wspolrzedneX[i] += punktOdniesieniaX;
			wspolrzedneY[i] += punktOdniesieniaY;
		}
		nameX += punktOdniesieniaX;
		nameY += punktOdniesieniaY;
		
		key = new Polygon(wspolrzedneX, wspolrzedneY, liczbaWierzcholkow);
		
		// Rysowanie klawisza
		g.setColor(kolorKlawisza);
		g.fillPolygon(key);
		g.setColor(Color.black);
		g.drawPolygon(wspolrzedneX, wspolrzedneY, liczbaWierzcholkow);
		if (name != null) {
			g.drawString(name, nameX, nameY);
		}
	}
}