// Aleksandra, Matylda
package pl.edu.pw.fizyka.pojava.NeumannOlko;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Graj extends JPanel {
	
	private JButton home;
	private JSlider glosnosc;
	private JRadioButton notesNames;
	static final int SLIDER_MIN = 10;
	static final int SLIDER_MAX = 100;
	static final int SLIDER_INIT = 55;

	public Graj(StrategiaGranie strategia) {
		notesNames= new JRadioButton("Wy�wietl nazwy dzwi�k�w");
		notesNames.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				strategia.getPiano().setRenderNames(notesNames.isSelected());
				strategia.getPiano().repaint();
			}
		});
		this.add(notesNames, BorderLayout.LINE_END);
	
		glosnosc = new JSlider(JSlider.HORIZONTAL, SLIDER_MIN, SLIDER_MAX, SLIDER_INIT); 
		glosnosc.addChangeListener(new ChangeListener() {
		
			@Override
			public void stateChanged(ChangeEvent e) {
				strategia.setVolume(glosnosc.getValue());
			}
			
		});	   
		glosnosc.setMinorTickSpacing(20);  
		glosnosc.setMajorTickSpacing(20);  
		glosnosc.setPaintTicks(true);  
		glosnosc.setPaintLabels(true);  
		strategia.setVolume(SLIDER_INIT);
		this.add(glosnosc, BorderLayout.CENTER);
	   	   
		home = new JButton("Powr�t");
		home.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFrame f = new StronaG();
				f.setVisible(true);	
			}
		});
		this.add(home, BorderLayout.LINE_START);
	   
		notesNames.setBackground(Color.white);
		glosnosc.setBackground(Color.white);
		this.setBackground(Color.white);
	   
		JLabel l2 = new JLabel();
		l2.setIcon(new ImageIcon("obrazki/tloG.jpg"));
		add(l2);
	    
	}
}