// Aleksandra, Matylda
package pl.edu.pw.fizyka.pojava.NeumannOlko;

import java.awt.event.MouseAdapter;

public class StrategiaGry extends MouseAdapter {
	Klawiatura piano;
	int volume = 55;
	
	public void setPiano(Klawiatura k) {
		piano = k;
	}
	
	public Klawiatura getPiano() {
		return piano;
	}
	
	public void setVolume(int v) {
		volume = v;
	}
}
