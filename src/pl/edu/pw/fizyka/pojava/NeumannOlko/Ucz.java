// Aleksandra, Matylda
package pl.edu.pw.fizyka.pojava.NeumannOlko;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Ucz extends JPanel {
	
	private JPanel p1, p2;
	private JButton home, play, stop;
	private JSlider glosnosc;
	private JComboBox<String> combobox;
	static final int SLIDER_MIN = 10;
	static final int SLIDER_MAX = 100;
	static final int SLIDER_INIT = 55;
	int numerNuty = 0;

	int[] dlaElizy = {20, 19, 20, 19, 20, 15, 18, 16, 13, 4, 8, 13, 15, 8, 12, 15, 16, 8, 20, 19, 20, 19, 20, 15, 18, 16, 13, 4, 8, 13, 15, 8, 16, 15, 13};
	int[] kurki = {4, 4, 11, 11, 13, 13, 11, 9, 9, 8, 8, 6, 6, 4, 11, 11, 9, 9, 8, 8, 6, 6, 11, 11, 9, 9, 8, 8, 6, 6, 4, 4, 11, 11, 13, 13, 11, 9, 9, 8, 8, 6, 6, 4};
	int[] godfather = {13, 18, 21, 20, 18, 21, 18, 20, 18, 14, 16, 13, 13, 18, 21, 20, 18, 21, 18, 20, 18, 13, 12, 11, 11, 14, 17, 20, 11, 14, 17, 18, 6, 6, 16, 14, 13, 16, 14, 14, 13, 13, 5, 6};
	int[] pustaMelodia = {};

	public Ucz(StrategiaNauka strategia) {
		
	    p1 = new JPanel();
	    this.add(p1);
	    setLayout(new BorderLayout());
	    
	    add(BorderLayout.NORTH, p1);
	    p2 = new JPanel();
	    this.add(p2);
	    add(BorderLayout.CENTER, p2);
	   
	    String[] nazwyMelodii = {"Dla Elizy","Kurki trzy","Ojciec chrzestny"};
	    int[][] melodie = {dlaElizy, kurki, godfather};
	   
	    combobox = new JComboBox<String>(nazwyMelodii);
	    p1.add(combobox, BorderLayout.LINE_END);
	
	    glosnosc = new JSlider(JSlider.HORIZONTAL, SLIDER_MIN, SLIDER_MAX, SLIDER_INIT); 
	    glosnosc.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				strategia.setVolume(glosnosc.getValue());
			}
	    	
	    });
	    glosnosc.setMinorTickSpacing(20);  
	    glosnosc.setMajorTickSpacing(20);  
	    glosnosc.setPaintTicks(true);  
	    glosnosc.setPaintLabels(true);  
	    glosnosc.setBackground(Color.white);
	    strategia.setVolume(SLIDER_INIT);
	    p1.add(glosnosc, BorderLayout.CENTER);
	   
	    home = new JButton("Powr�t");
	    home.addActionListener(new ActionListener() {
			 @Override
			 public void actionPerformed(ActionEvent e) {
				 JFrame f = new StronaG();
				 f.setVisible(true);
			 }
	    });
	    p1.add(home, BorderLayout.LINE_START);
	    p1.setBackground(Color.white);
	    
	    play = new JButton();
	    play.addActionListener(new ActionListener() {
	  	    @Override
		    public void actionPerformed(ActionEvent e) {
			    strategia.setMelody(melodie[combobox.getSelectedIndex()]);
		    }
	    });	
	    play.setPreferredSize(new Dimension(120, 80));
	    play.setIcon(new ImageIcon("obrazki/play.jpg"));
		play.setHorizontalTextPosition(JLabel.CENTER);
		
	    stop = new JButton();
	    stop.addActionListener(new ActionListener() {
	  	    @Override
		    public void actionPerformed(ActionEvent e) {
			    strategia.setMelody(pustaMelodia);
		    }
	    });	
	    stop.setPreferredSize(new Dimension(120, 80));
	    stop.setIcon(new ImageIcon("obrazki/stop.jpg"));
		stop.setHorizontalTextPosition(JLabel.CENTER);
		
	    p2.add(play);
	    p2.add(stop);
	    p2.setBackground(Color.white);
	}
	
	public class SliderChangeListener implements ChangeListener{
		
		@Override
		public void stateChanged(ChangeEvent arg0) {
			
		}
	}
	
	public Ucz(LayoutManager layout) {
		super(layout);
	}

	public Ucz(boolean isDoubleBuffered) {
		super(isDoubleBuffered);
	}

	public Ucz(LayoutManager layout, boolean isDoubleBuffered) {
		super(layout, isDoubleBuffered);
	}
	
}
