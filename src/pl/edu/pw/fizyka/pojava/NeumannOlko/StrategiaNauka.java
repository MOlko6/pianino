// Aleksandra, Matylda
package pl.edu.pw.fizyka.pojava.NeumannOlko;

import java.awt.Color;
import java.awt.event.MouseEvent;

public class StrategiaNauka extends StrategiaGry {
	int[] melody = {};
	int numerNuty = 0;
	
	public void setMelody(int[] m) {
		melody = m;
		numerNuty = 0;
		// Poni�sza p�tla: �eby nie zostawa�y kolory z poprzedniej melodii
		for (int i = 0; i < piano.keyboard.length; i++) {
    		piano.keyboard[i].setKolorKlawisza(piano.keyboard[i].getDomyslnyKolor());
    	}
		// If sprawdza czy nie sko�czy�a si� melodia
		if (numerNuty < melody.length) {
	    	piano.keyboard[melody[numerNuty]].setKolorKlawisza(Color.blue);
	 	   	numerNuty++;
    	}
		piano.repaint();
	}
	
	public void mousePressed(MouseEvent e) {
		for (int i = 0; i < piano.keyboard.length; i++) {
			if (piano.keyboard[i].contains(e.getPoint())) {
				piano.playSound(piano.soundsDir + "/" + piano.sounds[i], volume);
				piano.keyboard[i].setKolorKlawisza(Color.green);
				piano.repaint();
    		}
		}	
	}
		    
    public void mouseReleased(MouseEvent e) {
    	for (int i = 0; i < piano.keyboard.length; i++) {
    		piano.keyboard[i].setKolorKlawisza(piano.keyboard[i].getDomyslnyKolor());
    	}
    	if (numerNuty < melody.length) {
	    	piano.keyboard[melody[numerNuty]].setKolorKlawisza(Color.blue);
	 	   	numerNuty++;
    	}
 	   	piano.repaint();	
	}
}
